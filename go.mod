module manual-qa-testing-go-for-caleb

go 1.20

require (
	github.com/alecthomas/units v0.0.0-20211218093645-b94a6e3cc137 // indirect
	github.com/azure/azure-sdk-for-go v68.0.0+incompatible // indirect
	github.com/gorhill/cronexpr v0.0.0-20180427100037-88b0669f7d75 // indirect
)
